# infrastructure

1.Install Terraform on your local machine if you haven't already done so. You can download it from the official website: https://www.terraform.io/downloads.html

2.Create a new directory on your local machine where you will store the Terraform configuration files for your EC2 instance.

3.Create a new file in this directory called "main.tf". This file will contain the main configuration for your EC2 instance.

4.In your "main.tf" file, you will need to specify the provider (in this case, AWS) and the resource (in this case, EC2 instance) you want to create.

5.You will also need to create an AWS access key and secret key to authenticate Terraform with your AWS account. You can store these keys in environment variables or in a separate file called "terraform.tfvars".

6.Once you have your configuration files set up, you can initialize your Terraform project by creating .gitlab-ci.yml file
