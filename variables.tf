variable "ec2_region" {
  default = "eu-west-1"
}

variable "ec2_image" {
  default = "ami-09fd16644beea3565"
}

variable "ec2_instance_type" {
  default = "t2.micro"
}

variable "ec2_keypair" {
  default = "eu-west1"
}

variable "ec2_tags" {
  default = "Demo-Terraform-1"
}

variable "ec2_count" {
  default = "1"
}
